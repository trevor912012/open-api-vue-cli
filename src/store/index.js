import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
import router from '@/router'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 左側選單狀態
    leftSidebarOpen: false,
    // 登入的使用者資訊
    username: "",
    fullname: "",
    role: "",
    access_token:"",
    loginStatus:"",
    // 註冊的使用者資訊
    registerStatus:false,
    // 左側選單列表
    leftMenu: [],
    // 修改使用者燈箱資訊
    editModalAccount: "",
    editModalName: "",
    editModalRole: "",
    editModalPhone: "",
    editModalState: "",
    // 修改角色燈箱資訊
    editModalRoleCode: "",
    editModalRoleDesc: "",
    editModalRoleState: "",
    //使用者資料維護
    userAccountKeyWord: "",
    userNameKeyWord: "",
    //驗證伺服器URL
    AUTH_SERVICE_URL: process.env.VUE_APP_AUTH_SERVICE_URL,
  },
  getters: {
    get_leftSidebarOpen(state) {
      return state.leftSidebarOpen;
    },
    get_userName(state) {
      return state.username;
    },
    get_fullName(state) {
      return state.fullname;
    },
    get_role(state) {
      return state.role;
    },
    get_accessToken(state) {
      return state.access_token;
    },
    get_loginStatus(state) {
      return state.loginStatus;
    },
    get_registerStatus(state) {
      return state.registerStatus;
    },
    get_leftMenu(state) {
      return state.leftMenu;
    },
    get_editModalAccount(state) {
      return state.editModalAccount;
    },
    get_editModalName(state) {
      return state.editModalName;
    },
    get_editModalRole(state) {
      return state.editModalRole;
    },
    get_editModalPhone(state) {
      return state.editModalPhone;
    },
    get_editModalState(state) {
      return state.editModalState;
    },
    get_userAccountKeyWord(state) {
      return state.userAccountKeyWord;
    },
    get_userNameKeyWord(state) {
      return state.userNameKeyWord;
    },
    get_editModalRoleCode(state) {
      return state.editModalRoleCode;
    },
    get_editModalRoleDesc(state) {
      return state.editModalRoleDesc;
    },
    get_editModalRoleState(state) {
      return state.editModalRoleState;
    },
  },
  mutations: {
    mut_leftSidebarToggle(state) {
      state.leftSidebarOpen = !state.leftSidebarOpen
    },
    mut_leftSidebarManual(state, val) {
      state.leftSidebarOpen = val
    },
    mut_userName(state, val) {
      state.username = val
    },
    mut_fullName(state, val) {
      state.fullname = val
    },
    mut_role(state, val) {
      state.role = val
    },
    mut_accessToken(state, val) {
      state.access_token = val
    },
    mut_loginStatus(state, val) {
      state.loginStatus = val
    },
    mut_registerStatus(state, val) {
      state.registerStatus= val
    },
    mut_leftMenu(state, val) {
      state.leftMenu = val
    },
    mut_editModalAccount(state, val) {
      state.editModalAccount = val;
    },
    mut_editModalName(state, val) {
      state.editModalName = val;
    },
    mut_editModalRole(state, val) {
      state.editModalRole = val;
    },
    mut_editModalPhone(state, val) {
      state.editModalPhone = val;
    },
    mut_editModalState(state, val) {
      state.editModalState = val;
    },
    mut_userAccountKeyWord(state, val) {
      state.userAccountKeyWord = val;
    },
    mut_userNameKeyWord(state, val) {
      state.userNameKeyWord = val;
    },
    mut_roleCodeKeyWord(state, val) {
      state.editModalRoleCode = val;
    },
    mut_roleDescKeyWord(state, val) {
      state.editModalRoleDesc = val;
    },
    mut_roleDescStateKeyWord(state, val) {
      state.editModalRoleState = val;
    },
  },
  actions: {
    act_login({ commit, dispatch }, [username, password]) {
      let accountData = {
        username: username,
        password: password
      };
      axios.get(this.state.AUTH_SERVICE_URL + "/openapi/auth/login", {
        params: {
          username: accountData.username,
          password: accountData.password
        }
      }).then(({ data }) => {
          commit("mut_userName", data.username);
          commit("mut_fullName", data.fullname);
          commit("mut_role", data.role);
          commit("mut_accessToken", data.access_token);
      }).then(() => {
          if(this.state.role !== 'ROLE_dev'){
            dispatch("act_leftMenu")
          }
      }).then(() => {
          console.log("data.role:" + this.state.role);
          if(this.state.role === 'ROLE_dev'){
            router.push({ name: "Api_Portal" });
          }else{
            router.push({ name: "User_Management" });
          }
          
      }).catch((error) => {
          console.error(error);
          if(error.response.status === 500){
            commit("mut_loginStatus", "500");
          }
          
      });
    },
    act_logOut({ commit }) {
      commit("mut_userName", "")
      commit("mut_fullName", "")
      commit("mut_role", "")
      commit("mut_accessToken", "")
      commit("mut_loginStatus", "")
      commit("mut_registerStatus", "")
      commit("mut_leftMenu", [])
      commit("mut_editModalAccount", "")
      commit("mut_editModalName", "")
      commit("mut_editModalRole", "")
      commit("mut_editModalPhone", "")
      commit("mut_editModalState", "")
      commit("mut_userAccountKeyWord", "")
      commit("mut_userNameKeyWord", "")
      commit("mut_roleCodeKeyWord", "")
      commit("mut_roleDescKeyWord", "")
      commit("mut_roleDescStateKeyWord", "")
      router.push({ name: "Login" })
    },
    act_register({ commit, dispatch }, [username, password, fullName, callPhone]) {
      let accountData = {
        username: username,
        password: password,
        fullName: fullName,
        callPhone: callPhone
      };
      console.log("username:" + accountData.username);
      axios.post(this.state.AUTH_SERVICE_URL + "/openapi/auth/register", null, {
        params: {
          username: accountData.username,
          password: accountData.password,
          fullName: accountData.fullName,
          callPhone: accountData.callPhone
        }
      }).then(() => {
          commit("mut_registerStatus", true);
          //取回資料(成功)
          alert("註冊成功");
      }).then(() => {
        router.push({ name: "Login" });
    }).catch((error) => {
          console.error(error);
          if(error.response.status === 500){
            commit("mut_registerStatus", false);
          }
          
      });
    },
    act_leftMenu({ commit }) {
      axios.get(this.state.AUTH_SERVICE_URL + "/openapi/auth/menu", {
          headers: {
            'Authorization': 'Bearer ' + this.state.access_token
          }
        })
        .then(({ data }) => {
          commit("mut_leftMenu", data);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    act_leftMenuTopBtnToggle({ commit }) {
      $('.left-sidebar .lv1[id^="menucollapse_"]').collapse('hide');
      commit("mut_leftSidebarToggle");
    },
    // 將使用者資料修改資訊提交至 store
    act_get_editModalUserInfo({ commit }, data) {
      commit("mut_editModalAccount", data.username)
      commit("mut_editModalName", data.fullName)
      commit("mut_editModalRole", data.role)
      commit("mut_editModalPhone", data.cellPhone)
      commit("mut_editModalState", data.enabled)
    },

    // 將角色資料修改資訊提交至 store
    act_get_editModalRoleInfo({ commit }, data){
      console.log("role data:" + JSON.stringify(data))
      commit("mut_roleCodeKeyWord", data.role)
      commit("mut_roleDescKeyWord", data.roleDesc)
      commit("mut_roleDescStateKeyWord", data.status)
    }
  },
  modules: {
  },
})
