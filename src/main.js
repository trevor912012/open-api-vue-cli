import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// LocalStorage
import "./custom/local-storage";

// 開啟 vue 開發者工具
Vue.config.productionTip = false;

// 引入 axios
import axios from "axios";
import VueAxios from "vue-axios";
Vue.use(VueAxios, axios);

// 引入 SVGInjector
import SVGInjectorVue from "svginjector-vue";
Vue.use(SVGInjectorVue);

// 引入 vue easytable
import "vue-easytable/libs/theme-default/index.css";
import VueEasytable from "vue-easytable";
Vue.use(VueEasytable);

// 引入 vue2-perfect-scrollbar
import PerfectScrollbar from "vue2-perfect-scrollbar";
import "vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css";
Vue.use(PerfectScrollbar);

// 引入 bootstrap js, css 則是在 app.vue
import "bootstrap";

import BootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
Vue.use(BootstrapVue);
Vue.config.productionTip = false;
// 讓瀏覽器的全域環境可以使用到 $
import jQuery from "jquery";
window.$ = window.jQuery = jQuery;

//引入
import { ValidationProvider } from "vee-validate";
//啟用
Vue.component("ValidationProvider", ValidationProvider);
//位置:main.js
import { extend } from "vee-validate";
import { required } from "vee-validate/dist/rules";
extend("required", {
  ...required,
  message: "此欄位為必填欄位", //自訂的訊息
});

// 引入 VueClipboard
import VueClipboard from "vue-clipboard2";
VueClipboard.config.autoSetContainer = true;
Vue.use(VueClipboard);

// 引入swagger-ui
import SwaggerUI from "swagger-ui";
import "swagger-ui/dist/swagger-ui.css";
Vue.use(SwaggerUI);

import * as echarts from "echarts";
Vue.prototype.$echarts = echarts;

new Vue({
  router,
  store,
  render: function(h) {
    return h(App);
  },
}).$mount("#app");
